import { useState } from 'react';
import './App.css';
import Todo from './components/Todo'
import TodoForm from './components/TodoForm'

function App() {
  const [todos, setTodos] = useState([
    { text: "Demo Todo App" }
  ])
  const addTodo = text => {
    const newTodos = [...todos, { text }]
    setTodos(newTodos)
  }
  const removeTodo = index => {
    const newTodos = [...todos]
    newTodos.splice(index, 1)
    setTodos(newTodos)
  }
  return (
    <div className="app">
      <div className="todo-list">
      <h2>Todo App</h2>
        <TodoForm addTodo={addTodo}></TodoForm>
        {todos.map((todo, index) => (
          <Todo key={index} index={index} todo={todo}
            removeTodo={removeTodo}></Todo>
        ))}
      </div>
    </div>
  );
}

export default App;
