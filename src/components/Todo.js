import React from 'react'
import { useState } from 'react';


function Todo({ todo, index, removeTodo }) {
    const [value, setValue] = useState(todo.text)
    return (
        <div className="todo">
            <input className="edit" type="text" value={value}
                onChange={a => setValue(a.target.value)}></input>
            <button className="remove" onClick={() => removeTodo(index)}>x</button>
        </div>
    )
}
export default Todo