import React from 'react'
import { useState } from 'react';

function TodoForm({ addTodo }) {
    const [value, setValue] = useState("")

    const handleSubmit = e => {
        e.preventDefault()
        if (!value) {
            return
        }
        addTodo(value)
        setValue("")
    }
    return (
        <form className="form" onSubmit={handleSubmit}>
            <input className="input" type="text" placeholder="What do you want?" value={value}
                onChange={a => setValue(a.target.value)}></input>
            <input className="add" type="submit" value="Add"></input>
        </form>
    )
}
export default TodoForm